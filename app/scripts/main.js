(function (buttonEl, price2, currency2, nom) {
  /*globals QRCode*/
  'use strict';

  /* 3rd party:
   * https://bitcoinaverage.com/api
   * https://github.com/github/fetch (polyfill)
   * https://github.com/davidshimjs/qrcodejs
   */

  var qrWidget;
  var lastAvg = 0;
  var inputEl;

  var setDecimals = function(value, decimals) {
    var tens = Math.pow(10, decimals);
    return Math.floor(tens * value) / tens;
  };

  var zoz = function (aEl, price, currency) {
    var minPrice = 5;

    var setSearch = function (aEl2, obj) {
      var parts = aEl2.href.split('?');
      var leSearch = {};
      var ret = [];
      var r;

      parts[1].split('&').forEach(function (x) {
        var y = x.split('=');
        leSearch[y[0]] = y[1];
      });
      for (r in obj) { leSearch[r] = obj[r]; }
      for (r in leSearch) { ret.push(r + '=' + leSearch[r]); }
      parts[1] = ret.join('&');
      aEl2.href = parts.join('?');
    };

    var updateLink = function (avg) {
      var size;
      var qrEl = document.querySelector('div#qrcode') || document.createElement('div');
      var spanEl = document.querySelector('span.montant') || document.createElement('span');
      var amount;

      spanEl.classList.add('montant');
      if (price && avg) {
        amount = setDecimals(price / avg, 4);
        setSearch(aEl, {
          amount: amount,
          message: 'Don de ' + price + '$' + currency + ' à ' + nom
        });
        spanEl.innerHTML = 'Suggéré: ' + price + '$' + currency
          + ' (' + amount + ' btc)';
        aEl.insertBefore(spanEl, aEl.firstChild);
      }
      size = 320;

      if (qrEl.id === 'qrcode') { qrWidget.makeCode(aEl.href); }
      else {
        qrEl.id = 'qrcode';
        qrWidget = new QRCode(qrEl, {
          text: aEl.href,
          width: size,
          height: size,
          correctLevel: QRCode.CorrectLevel.M
        });
      }

      aEl.insertBefore(qrEl, aEl.firstChild);
    };

    var gotPrice = function () {
      fetch('https://api.bitcoinaverage.com/ticker/global/' + currency + '/24h_avg')
        .then(function (response) { return response.text(); })
        .then(function (avg) {
          inputEl = document.createElement('input');

          lastAvg = parseInt(avg, 10);
          if (lastAvg) {
            inputEl.type = 'range';
            inputEl.name = 'price';
            inputEl.value = price;
            inputEl.step = 5;
            inputEl.min = 5;
            inputEl.max = 200;

            inputEl.addEventListener('change', function () {
              price = parseInt(this.value, 10);
              updateLink(lastAvg);
            });
            aEl.parentNode.appendChild(inputEl);
          }
          updateLink(lastAvg);
        })
        .catch(function () { updateLink(); });
    };

    if (price && typeof price === 'number' && price >= minPrice) { gotPrice(); }
    else { updateLink(); }
  };

  var buttonClick = function buttonClick () {
    var letsdoit = function (data) {
      var wrapperEl = document.createElement('div');
      var closeEl = document.createElement('button');
      var aElNew = document.createElement('a');
      var btcAddress = data.addresses[Math.floor(data.addresses.length * Math.random())];
      var websocket = new WebSocket('wss://ws.blockchain.info/inv');
      var timer;

      var f1 = function(what, event) {
        var message;
        var inputs;
        var given;
        var textEl = document.createElement('div');
        var bodyEl = document.querySelector('body');
        var divEl = bodyEl.querySelector('div#don-bitcoin-qr a');
        var dollars;

        switch (what) {
        case 'open':
          websocket.send('{"op":"addr_sub", "addr":"' + btcAddress + '"}');
          timer = setInterval(function () { websocket.send('ping'); }, 30000);
          break;

        case 'close':
          clearInterval(timer);
          break;

        case 'message':
          message = JSON.parse(event.data).x;
          inputs = message.inputs.map(function (input) { return input.prev_out.addr; });
          given = message.out
            .filter(function (output) { return output.addr === btcAddress; })
            .map(function (output) { return output.value; })
            .reduce(function (p, c) { return p + c; });

          if (inputs.length && message.hash && given) {
            clearInterval(timer);
            websocket.close();
            dollars = setDecimals(given * lastAvg / 100000000, 2);
            textEl.innerHTML = '<h1>Merci!</h1><p>Reçu ' + dollars + '$' + currency2 + ' (' + setDecimals(given / 100000000, 4) + ' btc) de <small>' + inputs.join(' et ') + '</small> (<a title="hash: ' + message.hash + '" href="https://blockchain.info/tx/' + message.hash + '">transaction</a>)</p>';
            divEl.parentNode.replaceChild(textEl, divEl);
            inputEl.parentNode.removeChild(inputEl);
          }
          break;

        default:
          console.log('TYPE', what);
          console.log(event);
          if (what === 'error') {
            console.log('closing...');
            //FIXME (not very friendly, no explaination given...)
            closeEl.click();
          }
        }
      };

      aElNew.href = 'bitcoin:' + btcAddress + '?label=' + encodeURIComponent(nom);
      aElNew.innerHTML = ' adresse bitcoin de ' + nom + ' <span>' + btcAddress + '</span>';
      wrapperEl.id = 'don-bitcoin-qr';
      wrapperEl.appendChild(aElNew);
      closeEl.classList.add('close');
      closeEl.innerHTML = 'FERMER';
      closeEl.onclick = function () {
        wrapperEl.parentNode.removeChild(wrapperEl);
        clearInterval(timer);
        websocket.close();
      };
      wrapperEl.appendChild(closeEl);
      this.parentNode.appendChild(wrapperEl);
      websocket.onopen = f1.bind(null, 'open');
      websocket.onclose = f1.bind(null, 'close');
      websocket.onmessage = f1.bind(null, 'message');
      websocket.onerror = f1.bind(null, 'error');
      zoz(aElNew, price2, currency2);
    };

    fetch('/addresses.json')
      .then(function (response) { return response.json(); })
      .then(letsdoit.bind(this))
      .catch(function (err) { console.log('error fetching addresses', err); });
  };

  buttonEl.addEventListener('click', buttonClick);
}(document.querySelector('button#don-bitcoin'), 15, 'CAD', 'Robin Millette'));
