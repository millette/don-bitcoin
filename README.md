## Bitcoin, une monnaie virtuelle

### Versus Paypal

### Le Blockchain

## Implémentation côté client

### Configuration
Les première et dernière lignes de main.js:

```JavaScript
(function (buttonEl, price2, currency2, nom) {
  // ...
}(document.querySelector('button#don-bitcoin'), 15, 'CAD', 'Robin Millette'));
```

```buttonEl``` c'est l'élément qui déclenchera le processus,
```price2``` c'est le montant suggéré en dollars canadiens (selon le 3e argument, ```currency2```) et le dernier argument est le ```nom``` du destinataire.

### Adresse BTC aléatoire parmi 100
Paradoxe des anniversaires: Pour 100 adresses disponibles, risque qu'on donne la même adresse à deux personnes en même temps est de 50% si 12 personnes demandent une adresse simultanément.

Créer une centaines d'adressses BTC sous votre controle et mettez ça dans le fichier JSON "addresses.json" pour remplacer mes adresses.

## Prochaines étapes

### Côté serveur
Génération d'adresses BTC à partir de clés publiques pour complètement éviter le paradoxe des anniversaires.

## Références
* Cet article: <http://robin.millette.info/don-bitcoin-clientside>
* Exemple en fonction: <http://don.rollodeqc.com/>
* Sources: <https://gitlab.com/millette/don-bitcoin>

